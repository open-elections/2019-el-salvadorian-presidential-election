# 2019 El Salvadorian presidential election

    start_date: 2019-02-03
    end_date: 2019-02-03
    source: https://www.tse.gob.sv/2019/escrutinio-final/presidencial/index.html
    wikipedia: https://en.wikipedia.org/wiki/2019_Salvadoran_presidential_election